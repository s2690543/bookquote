package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception{
        Converter converter = new Converter();
        double price = converter.convert(1);
        Assertions.assertEquals(33.8, price, 0.4, "1 degree Celsius to Fahrenheit");
    }
}
