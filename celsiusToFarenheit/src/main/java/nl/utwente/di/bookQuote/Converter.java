package nl.utwente.di.bookQuote;

public class Converter {
    public double convert(double celsius) {
        double fahrenheit = celsius*1.8+32;
        return fahrenheit;
    }
}
